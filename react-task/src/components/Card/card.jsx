import React, { useState } from "react";
import "./card.css";
import Button from "../Button/button";
import CardInput from "../InCard/inCard";

const Card = ({ imgSrc, title, price, description, id, onAddToCart }) => {
  const [itemsCount, setItemsCount] = useState(1);

  const onNumberInputChange = (event) => {
    if (event.target.value !== "" && Number.isNaN(+event.target.value)) {
      setItemsCount(1);
    } else {
      setItemsCount(+event.target.value);
    }
  };

  return (
    <div className="card__block container__position">
      <div className="card__info container__position">
        <img src={imgSrc} alt={title} className="info__image" />
        <div className="info__description container__items">
          <div className="items__title container__items">
            <h6 className="logo__heading card__heading">{title}</h6>
            <p className="heading__price">
              $ {price} USD
            </p>
          </div>
          <p className="heading__text">
            {description}
          </p>
          <div className="info__buttons container__items">
            <CardInput
              value={itemsCount}
              onChange={onNumberInputChange}
            />
            <Button
              text="Add to cart"
              onClick={() => onAddToCart(id, itemsCount)}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Card;
