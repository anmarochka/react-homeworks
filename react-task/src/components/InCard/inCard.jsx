import React from "react";
import "./inCard.css";

const CardIn = ({ value, onChange }) => {
  return (
    <input
      className="CardInput"
      type="text"
      value={value}
      onChange={onChange}
    />
  );
};

export default CardIn;
