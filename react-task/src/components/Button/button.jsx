import React from "react";
import "./button.css";

const Button = ({ type = "primary", size = "tiny", onClick, text }) => {
  return (
    <button
      className={`Button Button--${type} Button--${size}`}
      onClick={onClick}
    >
      {text}
    </button>
  );
};

export default Button;

