import React from "react";
import "./cart.css";
import { ReactComponent as CartIcon } from "../../../assets/images/Busket.svg";

const Cart = ({ itemsCount }) => {
  return (
    <div className="Cart" data-items={itemsCount}>
      <CartIcon />
    </div>
  );
};

export default Cart;



