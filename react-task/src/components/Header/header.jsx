import React from 'react';
import './header.css';
import { ReactComponent as Logo } from "../../assets/images/Logo.svg";
import Cart from "../Header/Cart/cart";
import { Link } from 'react-router-dom';

const Header = ({ productsInCart }) => {
    return (
        <header className="header container__position">
            <div className="header__container container__items">
                <Logo className="logo"/>
                <div className="header__navigation">
                    <div>
                        <ul className="navigation__list">
                            <Link className="list__item" to="/Home">Home</Link>
                            <Link className="list__item" to="/Menu">Menu</Link>
                            <Link className="list__item" to="/Company">Company</Link>
                            <Link className="list__item" to="/Login">Login</Link>
                        </ul>
                    </div>
                    <Cart itemsCount={productsInCart} />
                </div>
            </div>
        </header>
    );
};

export default Header;