import React from 'react';
import './footer.css';
import Links from "./Links/links";
import { ReactComponent as Logo } from "../../assets/images/Logo.svg";
import { ReactComponent as InstagramIcon } from "../../assets/images/Instagram.svg";
import { ReactComponent as TwitterIcon } from "../../assets/images/Twitter.svg";
import { ReactComponent as YoutubeIcon } from "../../assets/images/Youtube.svg";
import {
    template,
} from '../../constants';

const Footer = () => {
    return (
        <footer className="footer container__position">
            <div className="footer__container container__items">
                <div className="footer__list container__items">
                    <div className="list__logo">
                        <Logo/>
                        <h6 className="logo__heading">
                            Takeaway & Delivery template 
                        </h6>
                        <p className="logo__text">
                            for small - medium businesses.
                        </p>
                    </div>
                    <div className="list">
                        <div className='list__blocks'>
                            <ul className="blocks__items">
                                <li className='items__text'>Company</li>
                                <li className='items__text'>Home</li>
                                <li className='items__text'>Order</li>
                                <li className='items__text'>FAQ</li>
                                <li className='items__text'>Contact</li>
                            </ul>
                        </div>
                        <Links items={template} blockClassName="list__blocks" className="blocks__items" liClassName="items__text"/>
                        <div className='list__blocks'>
                            <ul className="blocks__items">
                                <li className='items__text'>Flowbase</li>
                                <li className='items__text'>More Cloneables</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="footer__licence container__items">
                    <p className="licence__text">Built by <a href="#root">Flowbase</a> · Powered by <a href="#root">Webflow</a></p>
                    <div className="licence__icons container__items">
                        <div className="container__position icons__container">
                            <InstagramIcon/>
                        </div>
                        <div className="container__position icons__container">
                            <TwitterIcon/>
                        </div>
                        <div className="container__position icons__container">
                            <YoutubeIcon/>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    );
};

export default Footer;
