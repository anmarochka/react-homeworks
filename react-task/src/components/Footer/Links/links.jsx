import React from 'react';

const Links = ({ items, className, liClassName, blockClassName, linkClassName }) => {
  const renderUpperCaseFirstItem = (item, index) => {
    if (index === 0) {
      return item.toUpperCase();
    }
    return item;
  };

  const renderItem = (item, index) => {
    if (index === 0) {
      return <span>{renderUpperCaseFirstItem(item.text, index)}</span>;
    }
    return <a href={item.url} className={linkClassName}>{item.text}</a>;
  };

  return (
    <div className={blockClassName}>
      <ul className={className}>
        {items.map((item, index) => (
          <li key={index} className={liClassName}>
            {renderItem(item, index)}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Links;
