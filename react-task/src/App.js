import React, { useState, useEffect } from "react";
import "./App.css";
import  Header  from "./components/Header/header";
import  Footer  from "./components/Footer/footer";
import  CardMenu  from "./pages/CardMenu/CardMenu";
import Home from "./pages/Home/Home"
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";


const App = () => {
  const [productsInCart, setProductsInCart] = useState([]);

  useEffect(() => {
    const fetchOrders = async () => {
      const response = await fetch(
        "https://65de35f3dccfcd562f5691bb.mockapi.io/api/v1/orders"
      );
      const data = await response.json();
      setProductsInCart(data[0].meals);
    };
    fetchOrders();
  }, []);

  const onAddToCart = (id, amount) => {
    setProductsInCart((prevProductsInCart) => [
      ...prevProductsInCart,
      { id, amount },
    ]);
  };

  const totalProductsInCart = productsInCart.reduce(
    (acc, current) => acc + current.amount,
    0
  );

  return (
    <Router>
      <div className="App">
        <Header productsInCart={totalProductsInCart} />
        <div className="App__content container__position">
          <div className="background"></div>
          <Routes>
            <Route path="/menu" element={<CardMenu onAddToCart={onAddToCart} />} />
            <Route path="/home" element={<Home />} />
          </Routes>
        </div>
        <Footer />
      </div>
    </Router>
  );
};

export default App;