import React, { useState } from 'react';
import './StarRating.css'; // Создайте файл стилей StarRating.css

const StarRating = ({ totalStars }) => {
  const [rating, setRating] = useState(0);
  const handleStarClick = (starIndex) => {
    setRating(starIndex + 1);
  };

  return (
    <div className="star-rating">
  {[...Array(totalStars)].map((_, index) => (
    <span
      text='Trustpilot'
      key={index}
      onClick={() => handleStarClick(index)}
      className={index < rating ? 'star-filled' : 'star-empty'}
      aria-label={index < rating ? 'filled star' : 'empty star'}
      role="button"
      
    >
      {'\u2605'}
    </span>
  ))}
</div>

  );}

  export default StarRating;