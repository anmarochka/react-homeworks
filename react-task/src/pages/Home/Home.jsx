import React, { useEffect } from "react";
import "./Home.css";
import image from "../../assets/images/trustpilot.png"
import "./StarRating/StarRating"

// Функция для логирования API запросов
const apiLogger = (url, options, response) => {
  const apiData = {
    url,
    requestPayload: options.body,
    responseStatus: response.status,
    responseBody: response.clone().text()
  };
  localStorage.setItem('apiLog', JSON.stringify(apiData));
};

// Функция для обработки API запросов с логированием
const handleFetch = (url, options) => {
  return fetch(url, options)
    .then(response => {
      apiLogger(url, options, response);
      return response;
    })
    .catch(error => {
      console.error('API Error:', error);
      throw error;
    });
};

const Container = ({ title, content, buttonText, rating }) => {
  return (
    <div className="container">
      <h2>Beautiful food & takeaway,<mark  className="deliv">delivered </mark> to your door.</h2>
      <p>{content}</p>
      {buttonText && <button className="custom-button">{buttonText}</button>}
      {rating && (
        <div className="rating-container">
          <div>
            <img  className="star" src={image} alt={""}/>
          </div >
          <span>{rating}</span> based on 2000+ reviews
        </div>
      )}
    </div>
  );
};

const Home = () => {
  useEffect(() => {
    // Пример API вызова
    handleFetch('https://jsonplaceholder.typicode.com/posts/1', { method: 'GET' });
  }, []);

  return (
    <div className="home-container">
      <Container 
        content="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500."
        buttonText="Place an Order"
        rating="4.8 out of 5"
      />
      <div className="container-sec">
        <img src="https://s3-alpha-sig.figma.com/img/2c25/9012/abaa6e88befe4c3250b30702a9c9a25c?Expires=1717977600&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=j0mVC~PCKq13oDJvU7BakjPX2IJGLX0chVRivGyJb7zfLQfYGKCaerbh3ObJEnziE5CJCGZUZRErX7OzeOHugDYG0zGZcwYY76L2~k82pbTDj1lB0O0ROQkNyFkmzgf3DOuNexTSvICGVbx-N5I6r-U3zcp4zCLAEUWvCp-i1BKuqrOMCLXE7q0QrWe4m6Q7CZtsfXqXXiMOqK7rYWNOTRAdp7WwfuPAZWR7g1AUGOmFOjTjOQ1WN0BT51HxPstNvy6VwPcUlERin76WFGmn~iHZ7cK22i8PbsghhSV7k6vqBgejI1GX6kZapg9kxqA3Pp3trZMifvlCYwo-GUckTQ__"/>
      </div>
    </div>
  );
};

export default Home;