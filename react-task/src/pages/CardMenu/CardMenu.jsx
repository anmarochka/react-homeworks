import React, { useState, useEffect } from "react";

import "./CardMenu.css";

import Button  from "../../components/Button/button";
import Card from "../../components/Card/card";
import CardHeader  from "../../pages/CardHeader/CardHeader";

const CardMenu = ({ onAddToCart }) => {
  const [products, setProducts] = useState([]);
  const [itemsToRender, setItemsToRender] = useState(6);
  const [filter, setFilter] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(
        "https://65de35f3dccfcd562f5691bb.mockapi.io/api/v1/meals"
      );
      const data = await response.json();
      setProducts(data);
    };
    fetchData();
  }, []);

  const filteredProducts = filter
    ? products.filter(product => product.category === filter)
    : products;

  const productsShown = filteredProducts.slice(0, itemsToRender);

  const loadMore = () => {
    setItemsToRender(prev => prev + 6);
  };

  return (
    <div className="CardMenu">
      <CardHeader onFilterChange={setFilter} />
      <div className="CardMenu__cards">
        {productsShown.map((product) => (
          <Card
            key={product.id}
            title={product.meal}
            description={product.instructions.slice(0, 100) + "...."}
            price={product.price}
            imgSrc={product.img}
            onAddToCart={onAddToCart}
          />
        ))}
      </div>
      <div className="CardMenu__see-more">
        {itemsToRender < filteredProducts.length && (
          <Button text="See more" size="normal" onClick={loadMore} />
        )}
      </div>
    </div>
  );
};

export default CardMenu;
