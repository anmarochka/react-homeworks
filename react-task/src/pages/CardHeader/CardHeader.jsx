import React, { useState } from "react";
import Button  from "../../../src/components/Button/button";

const CardHeader = ({ onFilterChange }) => {
    const [activeCategory, setActiveCategory] = useState('');

    const handleClick = category => {
        setActiveCategory(category);
        onFilterChange(category);
    };

    return (
        <div className="CardHeader__heading">
            <h1 className="CardHeader__title">Browse our menu</h1>
            <p>Use our menu to place an order online, or <span className="heading__phone">phone<span className="tooltiptext">111-222-333</span></span> our store to place a pickup order. Fast and fresh food.</p>
            <div className="CardHeader__block-buttons container__items">
                <Button 
                    text="Dessert"
                    size="big"
                    type={activeCategory === 'Dessert' ? 'primary' : 'secondary'}
                    onClick={() => handleClick('Dessert')}
                />
                <Button 
                    text="Dinner"
                    size="big"
                    type={activeCategory === 'Dinner' ? 'primary' : 'secondary'}
                    onClick={() => handleClick('Dinner')}
                />
                <Button 
                    text="Breakfast"
                    size="big"
                    type={activeCategory === 'Breakfast' ? 'primary' : 'secondary'}
                    onClick={() => handleClick('Breakfast')}
                />
            </div>
        </div>
    );
};

export default CardHeader;

